package com.tsc.jarinchekhina.tm.service;

import com.tsc.jarinchekhina.tm.api.repository.IUserRepository;
import com.tsc.jarinchekhina.tm.api.service.IUserService;
import com.tsc.jarinchekhina.tm.entity.User;
import com.tsc.jarinchekhina.tm.enumerated.Role;
import com.tsc.jarinchekhina.tm.exception.AbstractException;
import com.tsc.jarinchekhina.tm.exception.empty.*;
import com.tsc.jarinchekhina.tm.exception.user.AccessDeniedException;
import com.tsc.jarinchekhina.tm.exception.user.EmailRegisteredException;
import com.tsc.jarinchekhina.tm.exception.user.LoginExistsException;
import com.tsc.jarinchekhina.tm.util.DataUtil;
import com.tsc.jarinchekhina.tm.util.HashUtil;

import java.util.Optional;

public class UserService extends AbstractService<User> implements IUserService {

    private final IUserRepository userRepository;

    public UserService(final IUserRepository userRepository) {
        super(userRepository);
        this.userRepository = userRepository;
    }

    @Override
    public User create(final String login, final String password) throws AbstractException {
        if (DataUtil.isEmpty(login)) throw new EmptyLoginException();
        if (DataUtil.isEmpty(password)) throw new EmptyPasswordException();
        final User user = new User();
        user.setRole(Role.USER);
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(password));
        return userRepository.add(user);
    }

    @Override
    public User create(final String login, final String password, final String email) throws AbstractException {
        if (DataUtil.isEmpty(login)) throw new EmptyLoginException();
        if (DataUtil.isEmpty(password)) throw new EmptyPasswordException();
        if (DataUtil.isEmpty(email)) throw new EmptyEmailException();
        if (findByLogin(login).isPresent()) throw new LoginExistsException(login);
        if (findByEmail(email).isPresent()) throw new EmailRegisteredException(email);
        final User user = create(login, password);
        user.setEmail(email);
        return user;
    }

    @Override
    public User create(final String login, final String password, final Role role) throws AbstractException {
        if (DataUtil.isEmpty(login)) throw new EmptyLoginException();
        if (DataUtil.isEmpty(password)) throw new EmptyPasswordException();
        if (role == null) throw new EmptyRoleException();
        if (findByLogin(login).isPresent()) throw new LoginExistsException(login);
        final User user = create(login, password);
        if (user == null) return null;
        user.setRole(role);
        return user;
    }

    @Override
    public Optional<User> findByLogin(final String login) throws EmptyLoginException {
        if (DataUtil.isEmpty(login)) throw new EmptyLoginException();
        return userRepository.findByLogin(login);
    }

    @Override
    public Optional<User> findByEmail(final String email) throws EmptyEmailException {
        if (DataUtil.isEmpty(email)) throw new EmptyEmailException();
        return userRepository.findByLogin(email);
    }

    @Override
    public Optional<User> removeByLogin(final String login) throws EmptyLoginException {
        if (DataUtil.isEmpty(login)) throw new EmptyLoginException();
        return userRepository.removeByLogin(login);
    }

    @Override
    public Optional<User> lockByLogin(String login) throws AbstractException {
        if (DataUtil.isEmpty(login)) throw new EmptyLoginException();
        final Optional<User> user = findByLogin(login);
        if(!user.isPresent()) throw new AccessDeniedException();
        user.get().setLocked(true);
        return user;
    }

    @Override
    public Optional<User> unlockByLogin(String login) throws AbstractException {
        if (DataUtil.isEmpty(login)) throw new EmptyLoginException();
        final Optional<User> user = findByLogin(login);
        if(!user.isPresent()) throw new AccessDeniedException();
        user.get().setLocked(false);
        return user;
    }

    @Override
    public User setPassword(final String userId, final String password) throws AbstractException {
        if (DataUtil.isEmpty(userId)) throw new EmptyIdException();
        if (DataUtil.isEmpty(password)) throw new EmptyPasswordException();
        final Optional<User> user = findById(userId);
        if(!user.isPresent()) throw new AccessDeniedException();
        final String hash = HashUtil.salt(password);
        user.ifPresent(e -> e.setPasswordHash(hash));
        return user.get();
    }

    @Override
    public User updateUser(
            final String userId,
            final String firstName,
            final String lastName,
            final String middleName
    ) throws AbstractException {
        if (DataUtil.isEmpty(userId)) throw new EmptyIdException();
        final Optional<User> user = findById(userId);
        if(!user.isPresent()) throw new AccessDeniedException();
        user.ifPresent(e -> e.setFirstName(firstName));
        user.ifPresent(e -> e.setLastName(lastName));
        user.ifPresent(e -> e.setMiddleName(middleName));
        return user.get();
    }

}

package com.tsc.jarinchekhina.tm.api.repository;

import com.tsc.jarinchekhina.tm.api.IRepository;
import com.tsc.jarinchekhina.tm.entity.Task;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;

public interface ITaskRepository extends IRepository<Task> {

    void clear(String userId);

    void removeAllByProjectId(String projectId);

    List<Task> findAll(String userId);

    List<Task> findAll(String userId, Comparator<Task> comparator);

    List<Task> findAllByProjectId(String userId, String projectId);

    Task add(String userId, Task task);

    Optional<Task> findById(String userId, String id);

    Optional<Task> findByIndex(String userId, Integer index);

    Optional<Task> findByName(String userId, String name);

    Optional<Task> remove(String userId, Task task);

    Optional<Task> removeById(String userId, String id);

    Optional<Task> removeByIndex(String userId, Integer index);

    Optional<Task> removeByName(String userId, String name);

}

package com.tsc.jarinchekhina.tm.api.repository;

import com.tsc.jarinchekhina.tm.api.IRepository;
import com.tsc.jarinchekhina.tm.entity.Project;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;

public interface IProjectRepository extends IRepository<Project> {

    void clear(String userId);

    List<Project> findAll(String userId);

    List<Project> findAll(String userId, Comparator<Project> comparator);

    Project add(String userId, Project project);

    Optional<Project> findById(String userId, String id);

    Optional<Project> findByIndex(String userId, Integer index);

    Optional<Project> findByName(String userId, String name);

    Optional<Project> remove(String userId, Project project);

    Optional<Project> removeById(String userId, String id);

    Optional<Project> removeByIndex(String userId, Integer index);

    Optional<Project> removeByName(String userId, String name);

}

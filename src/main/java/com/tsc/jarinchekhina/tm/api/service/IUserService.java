package com.tsc.jarinchekhina.tm.api.service;

import com.tsc.jarinchekhina.tm.api.IService;
import com.tsc.jarinchekhina.tm.entity.User;
import com.tsc.jarinchekhina.tm.enumerated.Role;
import com.tsc.jarinchekhina.tm.exception.AbstractException;
import com.tsc.jarinchekhina.tm.exception.empty.EmptyEmailException;
import com.tsc.jarinchekhina.tm.exception.empty.EmptyIdException;
import com.tsc.jarinchekhina.tm.exception.empty.EmptyLoginException;

import java.util.Optional;

public interface IUserService extends IService<User> {

    User create(String login, String password) throws AbstractException;

    User create(String login, String password, String email) throws AbstractException;

    User create(String login, String password, Role role) throws AbstractException;

    Optional<User> findByLogin(String login) throws EmptyLoginException;

    Optional<User> findByEmail(String email) throws EmptyEmailException;

    Optional<User> removeByLogin(String login) throws EmptyLoginException;

    Optional<User> lockByLogin(String login) throws EmptyLoginException, AbstractException;

    Optional<User> unlockByLogin(String login) throws EmptyLoginException, AbstractException;

    User setPassword(String userId, String password) throws AbstractException;

    User updateUser(String userId, String firstName, String lastName, String middleName) throws AbstractException;

}

package com.tsc.jarinchekhina.tm.api.service;

import com.tsc.jarinchekhina.tm.api.IService;
import com.tsc.jarinchekhina.tm.entity.Project;
import com.tsc.jarinchekhina.tm.enumerated.Status;
import com.tsc.jarinchekhina.tm.exception.AbstractException;
import com.tsc.jarinchekhina.tm.exception.user.AccessDeniedException;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;

public interface IProjectService extends IService<Project> {

    void clear(String userId) throws AccessDeniedException;

    List<Project> findAll(String userId) throws AccessDeniedException;

    List<Project> findAll(String userId, Comparator<Project> comparator) throws AccessDeniedException;

    Project add(String userId, Project project) throws AccessDeniedException;

    Project create(String userId, String name) throws AbstractException;

    Project create(String userId, String name, String description) throws AbstractException;

    Optional<Project> findById(String userId, String id) throws AbstractException;

    Optional<Project> findByIndex(String userId, Integer index) throws AbstractException;

    Optional<Project> findByName(String userId, String name) throws AbstractException;

    Optional<Project> remove(String userId, Project project) throws AccessDeniedException;

    Optional<Project> removeById(String userId, String id) throws AbstractException;

    Optional<Project> removeByIndex(String userId, Integer index) throws AbstractException;

    Optional<Project> removeByName(String userId, String name) throws AbstractException;

    Project updateProjectById(String userId, String id, String name, String description) throws AbstractException;

    Project updateProjectByIndex(String userId, Integer index, String name, String description) throws AbstractException;

    Project startProjectById(String userId, String id) throws AbstractException;

    Project startProjectByIndex(String userId, Integer index) throws AbstractException;

    Project startProjectByName(String userId, String name) throws AbstractException;

    Project finishProjectById(String userId, String id) throws AbstractException;

    Project finishProjectByIndex(String userId, Integer index) throws AbstractException;

    Project finishProjectByName(String userId, String name) throws AbstractException;

    Project changeProjectStatusById(String userId, String id, Status status) throws AbstractException;

    Project changeProjectStatusByIndex(String userId, Integer index, Status status) throws AbstractException;

    Project changeProjectStatusByName(String userId, String name, Status status) throws AbstractException;

}

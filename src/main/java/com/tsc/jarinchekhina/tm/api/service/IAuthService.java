package com.tsc.jarinchekhina.tm.api.service;

import com.tsc.jarinchekhina.tm.entity.User;
import com.tsc.jarinchekhina.tm.enumerated.Role;
import com.tsc.jarinchekhina.tm.exception.AbstractException;
import com.tsc.jarinchekhina.tm.exception.user.AccessDeniedException;

public interface IAuthService {

    User getUser() throws AbstractException;

    String getUserId() throws AccessDeniedException;

    boolean isAuth();

    void checkRoles(Role... roles) throws AbstractException;

    void logout();

    void login(String login, String password) throws AbstractException;

    void registry(String login, String password, String email) throws AbstractException;

}

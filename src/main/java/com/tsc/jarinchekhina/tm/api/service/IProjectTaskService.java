package com.tsc.jarinchekhina.tm.api.service;

import com.tsc.jarinchekhina.tm.entity.Project;
import com.tsc.jarinchekhina.tm.entity.Task;
import com.tsc.jarinchekhina.tm.exception.AbstractException;
import com.tsc.jarinchekhina.tm.exception.user.AccessDeniedException;

import java.util.List;
import java.util.Optional;

public interface IProjectTaskService {

    void clearProjects(String userId) throws AccessDeniedException;

    List<Task> findAllTaskByProjectId(String userId, String projectId) throws AbstractException;

    Task bindTaskByProjectId(String userId, String projectId, String taskId) throws AbstractException;

    Task unbindTaskByProjectId(String userId, String taskId) throws AbstractException;

    Optional<Project> removeProjectById(String userId, String projectId) throws AbstractException;

}

package com.tsc.jarinchekhina.tm.command.project;

import com.tsc.jarinchekhina.tm.command.AbstractProjectCommand;
import com.tsc.jarinchekhina.tm.entity.Project;
import com.tsc.jarinchekhina.tm.enumerated.Role;
import com.tsc.jarinchekhina.tm.exception.AbstractException;
import com.tsc.jarinchekhina.tm.exception.entity.ProjectNotFoundException;
import com.tsc.jarinchekhina.tm.util.TerminalUtil;

import java.util.Optional;

public class ProjectShowByNameCommand extends AbstractProjectCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "project-show-by-name";
    }

    @Override
    public String description() {
        return "show project by name";
    }

    @Override
    public void execute() throws AbstractException {
        final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[SHOW PROJECT]");
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        final Optional<Project> project = serviceLocator.getProjectService().findByName(userId, name);
        if (!project.isPresent()) throw new ProjectNotFoundException();
        print(project.get());
    }

    @Override
    public Role[] roles() {
        return new Role[] {Role.USER};
    }

}

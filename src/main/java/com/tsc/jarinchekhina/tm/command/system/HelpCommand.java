package com.tsc.jarinchekhina.tm.command.system;

import com.tsc.jarinchekhina.tm.command.AbstractCommand;
import com.tsc.jarinchekhina.tm.util.DataUtil;

import java.util.Collection;

public class HelpCommand extends AbstractCommand {

    @Override
    public String arg() {
        return "-h";
    }

    @Override
    public String name() {
        return "help";
    }

    @Override
    public String description() {
        return "display list of possible actions";
    }

    @Override
    public void execute() {
        System.out.println("[HELP]");
        final Collection<AbstractCommand> commands = serviceLocator.getCommandService().getCommands();
        for (final AbstractCommand command : commands) {
            String result = "";
            if (!DataUtil.isEmpty(command.name())) result += command.name() + " ";
            if (!DataUtil.isEmpty(command.arg())) result += "(" + command.arg() + ") ";
            if (!DataUtil.isEmpty(command.description())) result += "- " + command.description();
            System.out.println(result);
        }
    }

}

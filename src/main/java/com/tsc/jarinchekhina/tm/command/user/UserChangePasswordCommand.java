package com.tsc.jarinchekhina.tm.command.user;

import com.tsc.jarinchekhina.tm.command.AbstractUserCommand;
import com.tsc.jarinchekhina.tm.exception.AbstractException;
import com.tsc.jarinchekhina.tm.util.TerminalUtil;

public class UserChangePasswordCommand extends AbstractUserCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "change-password";
    }

    @Override
    public String description() {
        return "change password";
    }

    @Override
    public void execute() throws AbstractException {
        final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[CHANGE PASSWORD]");
        System.out.println("ENTER NEW PASSWORD:");
        final String password = TerminalUtil.nextLine();
        serviceLocator.getUserService().setPassword(userId, password);
    }

}

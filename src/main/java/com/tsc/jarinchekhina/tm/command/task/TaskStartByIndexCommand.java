package com.tsc.jarinchekhina.tm.command.task;

import com.tsc.jarinchekhina.tm.command.AbstractTaskCommand;
import com.tsc.jarinchekhina.tm.entity.Task;
import com.tsc.jarinchekhina.tm.enumerated.Role;
import com.tsc.jarinchekhina.tm.exception.AbstractException;
import com.tsc.jarinchekhina.tm.exception.entity.TaskNotFoundException;
import com.tsc.jarinchekhina.tm.util.TerminalUtil;

public class TaskStartByIndexCommand extends AbstractTaskCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "task-start-by-index";
    }

    @Override
    public String description() {
        return "start task by index";
    }

    @Override
    public void execute() throws AbstractException {
        final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[START TASK]");
        System.out.println("ENTER INDEX:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Task task = serviceLocator.getTaskService().startTaskByIndex(userId, index);
        if (task == null) throw new TaskNotFoundException();
    }

    @Override
    public Role[] roles() {
        return new Role[] {Role.USER};
    }

}

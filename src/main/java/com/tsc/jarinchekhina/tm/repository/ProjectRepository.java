package com.tsc.jarinchekhina.tm.repository;

import com.tsc.jarinchekhina.tm.api.repository.IProjectRepository;
import com.tsc.jarinchekhina.tm.entity.Project;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class ProjectRepository extends AbstractRepository<Project> implements IProjectRepository {

    private Predicate<Project> predicateByUser (final String userId) {
        return e -> userId.equals(e.getUserId());
    }

    private Predicate<Project> predicateByName (final String name) {
        return e -> name.equals(e.getUserId());
    }

    @Override
    public void clear(final String userId) {
        entities.stream()
                .filter(predicateByUser(userId))
                .forEach(e -> entities.remove(e));
    }

    @Override
    public List<Project> findAll(final String userId) {
        return entities.stream()
                .filter(predicateByUser(userId))
                .collect(Collectors.toList());
    }

    @Override
    public List<Project> findAll(final String userId, final Comparator<Project> comparator) {
        return entities.stream()
                .filter(predicateByUser(userId))
                .sorted(comparator)
                .collect(Collectors.toList());
    }

    @Override
    public Project add(final String userId, final Project project) {
        project.setUserId(userId);
        entities.add(project);
        return project;
    }

    @Override
    public Optional<Project> findById(final String userId, final String id) {
        return entities.stream()
                .filter(predicateByUser(userId))
                .filter(predicateById(id))
                .findFirst();
    }

    @Override
    public Optional<Project> findByIndex(final String userId, final Integer index) {
        return entities.stream()
                .filter(predicateByUser(userId))
                .skip(index)
                .findFirst();
    }

    @Override
    public Optional<Project> findByName(final String userId, final String name) {
        return entities.stream()
                .filter(predicateByUser(userId))
                .filter(predicateByName(name))
                .findFirst();
    }

    @Override
    public Optional<Project> remove(final String userId, final Project project) {
        if (!userId.equals(project.getUserId())) return Optional.empty();
        entities.remove(project);
        return Optional.of(project);
    }

    @Override
    public Optional<Project> removeById(final String userId, final String id) {
        Optional<Project> project = findById(userId, id);
        project.ifPresent(e -> entities.remove(e));
        return project;
    }

    @Override
    public Optional<Project> removeByIndex(final String userId, final Integer index) {
        Optional<Project> project = findByIndex(userId, index);
        project.ifPresent(e -> entities.remove(e));
        return project;
    }

    @Override
    public Optional<Project> removeByName(final String userId, final String name) {
        Optional<Project> project = findByName(userId, name);
        project.ifPresent(e -> entities.remove(e));
        return project;
    }

}
